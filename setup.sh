#!/usr/bin/env bash
pkgs=(
# Fonts
    'fonts-terminus'
# Display Manager
    'xorg'
    'xinit'
    'xclip'
# Setup
    'xclip'
    'rofi'
    'xterm'
    'spectrwm'
# Desktop Apps
    'pcmanfm'
    'firefox-esr'
# Audio
    'pulseaudio'
# Network
    'network-manager'
# Custom
    'lxappearance'
# Printers
    'printer-driver-gutenprint'
    'system-config-printer'
)
if lspci | grep -E "NVIDIA|GeForce"; then
    pkgs+=("nvidia-driver")
fi
sudo apt install "${pkgs[@]}" -y
cp -r .xinitrc ~/.xinitrc
cp -r .spectrwm.conf ~/.spectrwm.conf